package com.tuempresa.restaurante.modelo;

import javax.persistence.*;

import org.openxava.annotations.*;

import lombok.*;

@Entity @Getter @Setter
public class Empleado extends Identificable{
	
	@Column(length=50) 
    @Required  
    String nombre;
	
	@Column(length=50) 
    @Required  
    String apellidos;
	
	@Column(length=9) 
    @Required  
    int telefono;
	
	@Embedded 
	Direccion direccion;
	 
	@ManyToOne(fetch=FetchType.LAZY, optional=true) 
	@DescriptionsList 
	Establecimiento establecimiento;

}
