package com.tuempresa.restaurante.modelo;

import javax.persistence.*;

import org.openxava.annotations.*;

import lombok.*;

@Entity @Getter @Setter
public class Mesa extends Identificable{
	
	@Column(length=2) 
    @Required  
    int numero;
	
	@Column(length=2) 
    @Required  
    int personas;
	
}
