package com.tuempresa.restaurante.modelo;

import javax.persistence.*;

import org.openxava.annotations.*;

import lombok.*;

@Entity @Getter @Setter
public class Establecimiento extends Identificable{
	
	@Column(length=50) 
    @Required  
    String nombre;
	
	@Column(length=9) 
    @Required  
    int telefono;
    
    @Embedded 
    Direccion direccion; 

}
