package com.tuempresa.restaurante.modelo;
 
import java.time.*;
import java.util.*;

import javax.persistence.*;

import org.openxava.annotations.*;
import org.openxava.calculators.*;

import lombok.*;
 
@Entity @Getter @Setter
@View(members=
	"anyo, fecha, mesa;" + 
	"datos {" + 
	    "cliente;" +
	    "detalles;" +
	    "observaciones" +
	"}"
)
abstract public class DocumentoComercial extends Identificable {

    @Column(length=4)
    @DefaultValueCalculator(CurrentYearCalculator.class) 
    int anyo;
 
    @Required
    @DefaultValueCalculator(CurrentLocalDateCalculator.class) 
    LocalDate fecha;
    
    @ManyToOne(fetch=FetchType.LAZY, optional=true) 
	Mesa mesa;
    
    @ManyToOne(fetch=FetchType.LAZY, optional=false) 
    @ReferenceView("Simple") 
    Cliente cliente;
    
    @ElementCollection
    @ListProperties("plato.numero, plato.nombre, cantidad, importe")
    Collection<Detalle> detalles;
 
    @Stereotype("MEMO")
    String observaciones;
 
}