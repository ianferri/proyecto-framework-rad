package com.tuempresa.restaurante.modelo;

import java.time.*;

import javax.persistence.*;

import org.openxava.annotations.*;
import org.openxava.calculators.*;

import lombok.*;

@Entity @Getter @Setter
@View(members=
"fecha, establecimiento;" +
"cliente;" +
"mesa"
)
public class Reservas extends Identificable{
	
	@ManyToOne(fetch=FetchType.LAZY, optional=true) 
	@DescriptionsList 
	Cliente cliente;
	
	@ManyToOne(fetch=FetchType.LAZY, optional=true) 
	Mesa mesa;
	
	@ManyToOne(fetch=FetchType.LAZY, optional=true) 
	@DescriptionsList 
	Establecimiento establecimiento;
	
	@Required
    @DefaultValueCalculator(CurrentLocalDateCalculator.class) 
    LocalDate fecha;

}
