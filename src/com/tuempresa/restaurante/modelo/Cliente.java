package com.tuempresa.restaurante.modelo;
 
import javax.persistence.*;

import org.openxava.annotations.*;

import lombok.*;
 
@Entity  
@Getter @Setter 
@View(name="Simple", 
	members="numero, nombre" 
)
public class Cliente {
 
    @Id  
    @Column(length=6)  
    int numero;
 
    @Column(length=50) 
    @Required  
    String nombre;
    
    @Column(length=50) 
    @Required  
    String apellidos;
    
    @Column(length=9) 
    @Required  
    int telefono;
    
    @Embedded 
    Direccion direccion; 
 
}