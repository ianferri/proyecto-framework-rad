package com.tuempresa.restaurante.modelo;
 
import java.math.*;

import javax.persistence.*;

import org.openxava.annotations.*;

import lombok.*;
 
@Embeddable @Getter @Setter
public class Detalle {
 
    int cantidad;
 
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    Plato plato;
    
    @Stereotype("DINERO")
    @Depends("plato.numero, cantidad")
    public BigDecimal getImporte() {
        if (plato == null || plato.getPrecio() == null) return BigDecimal.ZERO;
        return new BigDecimal(cantidad).multiply(plato.getPrecio());
    }
 
}